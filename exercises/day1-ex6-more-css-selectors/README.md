# More CSS Selectors

For this exercise, you will be changing existing styling as well as adding a few new ones using some of the discussed CSS selectors. You will also be asked to add a few CSS variables.

Please do note that since the exercises are geared towards the goal of building a portfolio website, we might only apply the lessons that would help us complete our website.

If you do want to try to use other CSS selectors in an exercise, you can look up exercises at https://codepen.io/

No `TODO` checklist will be provided for this exercise. All `TODOs` will be found on this exercise's `style.css` file.

Estimated time to complete: 35 minutes

Output for this exercise should almost be the same as the previous one with the addition of a few visual styling.