# CSS Flexbox

For this exercise, you will be making use of CSS flexbox to improve the layout of our website. You will be replacing most of the selectors with `display: inline-block` and convert them into flex containers as well as adding new ones.

No `TODO` checklist will be provided for this exercise. All `TODOs` will be found on this exercise's `style.css` file.

Estimated time to complete: 35 minutes

Output for this exercise should look similar to this:

![](../resources/output/flexbox-output.png)