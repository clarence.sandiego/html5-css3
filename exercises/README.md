# Revised HTML5-CSS3 Exercises

Exercises are for the revised 2-day HTML5/CSS3 course and is still **in progress**

## Day 1

1. HTML elements: headings, paragraphs, lists, links (\<a>)
2. CSS selectors: universal, #id, .class, type
3. CSS inheritance
4. More HTML elements: images, tables, forms, input types, sectioning tags
5. CSS Layout (normal flow and box model)
6. More CSS selectors

## Day 2

1. CSS Flexbox
2. CSS Grid
3. Responsive Web Design
4. Sass
5. Bootstrap


Local Machine Requirements

1. Installation of VSCode
2. Installation of VSCode extensions
	- Live Server (by Ritwick Dey)
	- Todo Tree (by Gruntfuggly)
	- Live Sass Compiler (by Glenn Marks)