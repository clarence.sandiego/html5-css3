# Responsive Web Design

For this exercise, you'll be applying basic media queries to make our simple portfolio website responsive. Please do note that this exercise might not reflect the best practices regarding using media queries but should be enough as a practice.

New elements (burger-menu and back-to-top buttons) were added. You will be asked to include them when applying styles using media queries.

In this exercise, we need to declare three breakpoints: 1200px, 768px, and 576px which are for medium-large, small, and extra small screens respectively.

We have provided a TODO list below as your guide in completing this exercise.

# TODO:

- [&nbsp;&nbsp;] Declare a `media query` for when the browser's width is `1200px and below`
    - [&nbsp;&nbsp;] Change the `flex-direction` of `.container` inside `#about` and `.contact-wrapper` so their flex items are displayed vertically
    - [&nbsp;&nbsp;] Add a `50px` row-gap to `.contact-wrapper`
    - [&nbsp;&nbsp;] Set the `width` of `.contact-details` and `.contact-form-wrapper` to 100%
    - [&nbsp;&nbsp;] Center the `<p>` text of `.profile-description`
    - [&nbsp;&nbsp;] Reduce the margin of the `<li>` in `.nav-list` to `12px`

- [&nbsp;&nbsp;] Declare a `media query` for when the browser's width is `768px and below`
    - [&nbsp;&nbsp;] `Hide .header-right` and `.back-to-top` using the `display` property
    - [&nbsp;&nbsp;] Use the approprite `display` value to make `burger-menu` visible
    - [&nbsp;&nbsp;] Change the `flex-direction` of `<footer>` so its items are displayed vertically

- [&nbsp;&nbsp;] Declare a `media query` for when the browser's width is `576px and below`
    - [&nbsp;&nbsp;] Align the `<p>` text of `.profile-description` to the left
    - [&nbsp;&nbsp;] Reduce the `font-size` of the 1st `<p>` in `.profile-description` to `48px`
    - [&nbsp;&nbsp;] Reduce the `font-size` of the 2nd `<p>` in `.profile-description` to `32px`

<br/>
Estimated time to complete: 35 minutes