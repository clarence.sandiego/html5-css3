# CSS Selectors

For this exercise, you'll be applying simple styles to HTML elements using the previously discussed CSS selectors.

Notice that elements are now grouped and wrapped around \<div>'s with classes for easier styling. Feel free to modify the class names and/or \<div>'s to your own preference.

You'll also be using fonts from Google Fonts. Fonts are already added using the `@import` rule. For more details, you can refer to this [link](https://stackoverflow.com/questions/14676613/how-to-import-google-web-font-in-css-file).

Use the appropriate selector and properties to complete the TODO list.

## TODO:

- [&nbsp;&nbsp;] `<body>` should have a `background-color` of `#ecf0f3`

- [&nbsp;&nbsp;] `<h1>` to `<h5>` should have a `font-weight` of `bold`

- [&nbsp;&nbsp;] `<h1>` should have a `font-size` of `40px`

- [&nbsp;&nbsp;] `<h2>` should have a `font-size` of `32px`

- [&nbsp;&nbsp;] `<h3>` should have a `font-size` of `28px`

- [&nbsp;&nbsp;] `<h4>` should have a `font-size` of `24px`

- [&nbsp;&nbsp;] `<h5>` should have a `font-size` of `20px`

- [&nbsp;&nbsp;] `<h1>` and `<h2>` should have a `font-family` of `'Montserrat', sans-serif`

- [&nbsp;&nbsp;] `<h3>`,`<h4>`,`<h5>`,`<p>`, and `<a>` should have a `font-family` of `'Poppins', sans-serif`

- [&nbsp;&nbsp;] `<span>` should have a `color` of `#ff014f`

- [&nbsp;&nbsp;] element with class `nav-list` should have `no list-style`

- [&nbsp;&nbsp;] `<a>`'s of elements with classes `social-links`,`section-items`, and `nav-list` should have `no text-decoration` and a `color` of `black`

- [&nbsp;&nbsp;] `<a>`'s of element with class `nav-list` shoud also have a `font-weight` of `600` and `uppercase` as the `text-transform`

- [&nbsp;&nbsp;] `<p>` of element with class `portfolio-content` should have a `color` of `#ff014f`, `font-weight` of `600`, `uppercase` as the `text-transform`, `font-size` of `12px`, and `letter-spacing` of `2px`

- [&nbsp;&nbsp;] `<p>` of element with class `contact-details` should have a `color` of `#ff014f` and a `font-size` of `18px`

<br/>
Estimated time to complete: 35 minutes

Output for this exercise should look similar to this:

![](../resources/output/css-selectors-output.png)