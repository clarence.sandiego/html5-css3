# More HTML Elements: images, forms, sectioning elements

For this exercise, you'll be adding additional elements to our portfolio website using the previously discussed HTML tags.

Use the appropriate tags to add images and form elements and to give our HTML clearer meaning for the browsers and other developers.

Use the icons and portfolio images inside the `resources` folder from the main folder.

*Note:* Stylings were preadded to style the elements that you'll be adding so you could focus on applying the lessons you learned on this topic.

## TODO:

- [&nbsp;&nbsp;] Wrap the appropriate `<div>` and its elements in a `<header>`
- [&nbsp;&nbsp;] Wrap the appropriate group of elements in a `<nav>`
- [&nbsp;&nbsp;] Wrap the appropriate `<div>`'s and its elements in a `<section>`. Assign an id to each section based on its purpose
- [&nbsp;&nbsp;] Wrap those `<section>`'s inside a `<main>` element
- [&nbsp;&nbsp;] Wrap the appropriate group of elements in a `<footer>`
- [&nbsp;&nbsp;] Add `profile.jpg` inside the `about` container. Give it a class `profile-img` then wrap it inside a `<div>`
- [&nbsp;&nbsp;] Replace the text for our social media links with the appropriate social media icons. Black icons for the first set of links then white icons for the second. (size: 32x32)
- [&nbsp;&nbsp;] Add an image icon on every `section-item-card <div>` inside the `services` container and give each a class `service-icon` (size: 80x80)
- [&nbsp;&nbsp;] Add a portfolio image on every `section-item-card <div>` inside the `portfolio` container and give each a class `portfolio-img` (size: 320x240)
- [&nbsp;&nbsp;] Add the appropriate labels and input elements inside the `contact-form-wrapper <div>` (with name and id and are required). Add a `<button>` at the end. `<div>`'s were preadded for your convenience.
- [&nbsp;&nbsp;] Wrap your form elements in a `<form>`

<br/>
Estimated time to complete: 45 minutes