# HTML elements: headings, paragraphs, lists, links

In this course, you'll be completing a static portfolio page as you go through each lab exercises.

The final output should look similar to the image file `portfolio.png`

For this exercise, will be starting with adding text content on our page using basic HTML elements.

## TODO:

- [ ] Using the appropriate HTML elements, add texts from the `content.txt` file to your website.

Estimated time to complete: 20 minutes

Output for this exercise should look similar to this:

![](../resources/output/html-elements-output.png)