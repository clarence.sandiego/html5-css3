# CSS Grid

For this exercise, you will be making use of simple CSS grid properties to improve our form.

The layout of our portfolio website doesn't need much of CSS grid so we won't be able to apply everything from the lesson.

If you do want to futher practice on CSS grid, here's a great interactive website that you can visit - https://cssgridgarden.com

No `TODO` checklist will be provided for this exercise. All `TODOs` will be found on this exercise's `style.css` file.

Estimated time to complete: 25 minutes

Output for this exercise should look similar to this:

![](../resources/output/grid-output.png)