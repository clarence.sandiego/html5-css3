# CSS Preprocessor - Sass

For this exercise, you'll be converting the existing `style.css` into a Sass file while following the SCSS syntax. You will be asked to use basic Sass features (variables, nesting, @mixins, partials).

To be able to compile the `.scss` file into a usable `.css`, kindly install `Live Sass Compiler by Glen Marks`. Once installed, restart VSCode if necessary and make sure to run the extension by clicking the "Watch Sass" button (can be found at the bottom of VSCode).

In this exercise, you will have the freedom to refactor the `.css` to `.scss` however you like as long as you're applying the previously discussed lesson on Sass features. 

Do note though that your final `.scss` file might be different from the next exercise's. You may copy your own changes to the next exercise's `.scss` accordingly.

Here's a TODO list to give you an idea on the things that you can do.

## TODO
- [&nbsp;&nbsp;] Convert CSS variables into `Sass variables`

- [&nbsp;&nbsp;] Apply DRY principle by moving some repeating blocks of code into a `@mixin`

- [&nbsp;&nbsp;] Identify parent and child elements and apply `nesting` when possible

- [&nbsp;&nbsp;] Make use of `partials`. You can move blocks of code with similar function into another Sass file (ex. `_variables.scss`). Do remember to use the `@use` rule (and `@forward` when necessary)

<br/>
Estimated time to complete: 45 minutes

Output should be the same as the previous exercise as we're only converting the existing `.css` and not adding any new styles.