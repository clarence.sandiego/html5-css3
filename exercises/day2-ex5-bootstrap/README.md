# CSS Framework - Boostrap

This will be the last exercise for this course. For this exercise, you'll be asked to use Bootstrap utility classes to make use of its grid system and form stylings to make our portfolio website more responsive.

Go through the TODO list to complete this exercise. Once done, go through the comments in the `style.scss` to help you override some simple Bootstrap stylings.

## TODO

- [&nbsp;&nbsp;] Import Bootstrap CSS and JS by adding the links to `index.html`

- [&nbsp;&nbsp;] Add the appropriate utility classes to our form labels, inputs, and button to apply Bootstrap styling

- [&nbsp;&nbsp;] Modify our `<div>`'s to make use of Bootstrap's grid system

    - [&nbsp;&nbsp;] Add a `row` class to the existing `<div>` with `class=section-items` to enable its children to use grid system
    
    - [&nbsp;&nbsp;] Add a `gutter` with a width of `5` to the `row`. Refer to https://getbootstrap.com/docs/5.0/layout/gutters/.

    - [&nbsp;&nbsp;] Wrap the existing `<div>`'s with `class=section-item-card` into a new `<div>`. This new `<div>` will make use of the grid system utility classes.

    - Add the appropirate classes to the new `<div>` so that:
        - [&nbsp;&nbsp;] It `spans 4` when the screen is `xl`
        - [&nbsp;&nbsp;] It `spans 6` when the screen is `lg` and `md`
        - [&nbsp;&nbsp;] It `spans 12` when the screen is `sm`

Output of this exercise should be similar to the `portfolio.png` in the main folder.
