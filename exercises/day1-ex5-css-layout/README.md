# CSS Layout (Normal flow and Box Model)

For this exercise, you'll be applying `box model` properties to some of the elements of our portfolio website. This is to primarily position and lay them out to look more like how it would look at the end of the course.

Do note that most of the display values you'll be applying are temporary as we'll make use of `Flexbox` and `Grid` on the 2nd day.

No TODO checklist will be provided for this exercise. All TODOs will be found on this exercise's `style.css` file.

Estimated time to complete: 60 minutes

Output for this exercise should look similar to this:

![](../resources/output/css-layout-output.png)