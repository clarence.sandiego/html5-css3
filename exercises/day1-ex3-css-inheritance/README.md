# CSS inheritance

In this lab exercise, you'll be styles to a basic HTML file to test CSS inheritance.
What you'll learn:

1. How CSS properties are inherited
2. Difference between inherited and non-inherited properties
3. Effects of using initial and inherit values

Estimated time to complete: 15 minutes

Note: Go through the exercise by following the TODOs. To view all TODOs as a list, you can download the Todo Tree extension in VSCode.